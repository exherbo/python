# Copyright 2008-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2020-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'PyQt4-4.4.4-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

myexparam sip_version

MY_PN=${PN,,}
MY_PV=${PV/_pre/.dev}
MY_PNV=${MY_PN}-${MY_PV}

require pypi

case "$(ever range 4)" in
    pre*)
        require python [ blacklist='2 3.8' multiunpack=true work=${PN}-${MY_PV} ]
        DOWNLOADS="https://www.riverbankcomputing.com/pypi/packages/${PN}/${PN}-${MY_PV}.tar.gz" ;;
    *)
        require python [ blacklist='2 3.8' multiunpack=true work=${MY_PNV} ]
        DOWNLOADS="https://pypi.python.org/packages/source/p/${MY_PN}/${MY_PNV}.tar.gz" ;;
esac


SUMMARY="PyQt6 is a set of Python bindings for the Qt6 toolkit"
DESCRIPTION="
PyQt is a set of Python bindings for the Qt application framework and runs
on all platforms supported by Qt including Windows, MacOS/X and Linux. The
bindings are implemented as a set of Python modules and contain over 300
classes and over 6,000 functions and methods.
"
BASE_URI="https://www.riverbankcomputing.com"
HOMEPAGE="${BASE_URI}/software/pyqt/download/$(ever range 4)"

UPSTREAM_RELEASE_NOTES="${BASE_URI}/news/pyqt-$(ever delete_all)"
UPSTREAM_DOCUMENTATION="${BASE_URI}/static/Docs/${PN}/ [[ lang = en ]]"

SLOT="0"
LICENCES="|| ( GPL-2 GPL-3 )"
MYOPTIONS="debug
    bluetooth     [[ description = [ Build bindings for QtBluetooth & QtNfc ] ]]
    multimedia    [[ description = [ Build bindings for QtMultimedia ] ]]
    pdf           [[ description = [ Build bindings for QtPdf{Quick,Widgets} ] ]]
    positioning   [[ description = [ Build bindings for QtPositioning ] ]]
    quick3d       [[ description = [ Build bindings for QtQuick3D ] ]]
    remoteobjects [[ description = [ Build bindings for QtRemoteObjects ] ]]
    sensors       [[ description = [ Build bindings for QtSensors ] ]]
    serialport    [[ description = [ Build bindings for QtSerialPort ] ]]
    speech        [[ description = [ Build bindings for QtSpeech ] ]]
    sql           [[ description = [ Build bindings for QtSql ] ]]
    statemachine  [[ description = [ Build bindings for QtStateMachine ] ]]
    webchannel    [[ description = [ Build bindings for QtWebChannel ] ]]
    websockets    [[ description = [ Build bindings for QWebSockets ] ]]
"

DEPENDENCIES="
    build:
        dev-python/PyQt6-sip[>=13.8][python_abis:*(-)?]
        dev-python/PyQt-builder[>=1.17&<2][python_abis:*(-)?]
    build+run:
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/sip[>=$(exparam sip_version)][python_abis:*(-)?]
        sys-apps/dbus
        x11-libs/qtbase:6
        x11-libs/qtdeclarative:6
        x11-libs/qtsvg:6
        x11-libs/qttools:6[assistant] [[ note = [ QtHelp ] ]]
        bluetooth? ( x11-libs/qtconnectivity:6 )
        multimedia? ( x11-libs/qtmultimedia:6 )
        pdf? ( x11-libs/qtwebengine:6 )
        positioning? ( x11-libs/qtlocation:6 )
        quick3d? ( x11-libs/qtquick3d:6 )
        remoteobjects? ( x11-libs/qtremoteobjects:6 )
        sensors? ( x11-libs/qtsensors:6 )
        serialport? ( x11-libs/qtserialport:6 )
        speech? ( x11-libs/qtspeech:6 )
        sql? ( x11-libs/qtbase:6[sql] )
        statemachine? ( x11-libs/qtscxml:6 )
        webchannel? ( x11-libs/qtwebchannel:6 )
        websockets? ( x11-libs/qtwebsockets:6 )
"

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}" )

pyqt_enable() {
    local flag=${1,,} module=${2:-Qt${1}}

    if option "${flag}" ; then
        echo "--enable=${module}"
    fi
}

# Unused modules:
# QAxContainer: Qt's ActiveX and COM support (qtactiveqt)
PYQT_CONFIGURE_OPTION_ENABLES=(
    Bluetooth
    "bluetooth QtNfc"
    Multimedia
    "multimedia QtMultimediaWidgets"
    "pdf QtPdf"
    "pdf QtPdfWidgets"
    Positioning
    Quick3D
    "remoteobjects QtRemoteObjects"
    Sensors
    SerialPort
    'speech QtTextToSpeech'
    Sql
    "statemachine QtStateMachine"
    "webchannel QtWebChannel"
    "websockets QtWebSockets"
)
PYQT_CONFIGURE_ALWAYS_ENABLES=(
    QtCore
    QtDBus
    QtGui
    QtHelp
    QtNetwork
    QtOpenGL
    QtOpenGLWidgets
    QtPrintSupport
    QtQml
    QtQuick
    QtQuickWidgets
    QtSvg
    QtSvgWidgets
    QtTest
    QtWidgets
    QtXml
)
PYQT_CONFIGURE_PARAMS=(
    # Disable QtDesigner support. Enable with: --enable=QtDesigner --enable=uic
    --no-designer-plugin
    --disabled-feature=PyQt_OpenGL_ES2
    --disabled-feature=PyQt_Vulkan
    # Only available for Apple or Android
    --disabled-feature=PyQt_Permissions
)

prepare_one_multibuild() {
    python_prepare_one_multibuild

    edo sed -e "s:'pkg-config':'$(exhost --tool-prefix)pkg-config':" \
            -i project.py
}

configure_one_multibuild() {
    edo ${PYTHON} /usr/$(exhost --target)/bin/sip-build \
        $(option debug && echo '--debug') \
        $(option debug && echo '--qml-debug') \
        --confirm-license \
        --no-make \
        --qmake /usr/$(exhost --target)/lib/qt6/bin/qmake \
        --target-dir $(python_get_sitedir) \
        --verbose \
        "${PYQT_CONFIGURE_PARAMS[@]}" \
        $(for s in ${PYQT_CONFIGURE_ALWAYS_ENABLES[@]} ; do echo "--enable=${s}" ; done) \
        $(for s in "${PYQT_CONFIGURE_OPTION_ENABLES[@]}" ; do pyqt_enable ${s} ; done)

    edo find "${WORK}" -name Makefile | xargs sed -i "/^\tstrip /d"
}

compile_one_multibuild() {
    edo cd build
    emake
}

install_one_multibuild() {
    edo pushd build
    emake -j1 INSTALL_ROOT="${IMAGE}" install
    python_bytecompile
    edo popd

    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/bin
    edo mv "${IMAGE}"/usr/bin/* "${IMAGE}"/usr/$(exhost --target)/bin
    edo rmdir "${IMAGE}"/usr/bin

    insinto /usr/share/doc/${PNVR}
    doins -r examples
}

