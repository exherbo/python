# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PN,,}-${PV}

require pypi
require py-pep517 [ backend=setuptools test=pytest entrypoints=[ qtpy ] work=${MY_PNV} ]

SUMMARY="Provides an uniform layer to support PyQt/PySide with a single codebase"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    providers: ( pyqt5 pyqt6 pyside2 pyside6 ) [[
        number-selected = at-least-one
    ]]
"

DEPENDENCIES="
    build+run:
        dev-python/packaging[python_abis:*(-)?]
        providers:pyqt5? ( dev-python/PyQt5[python_abis:*(-)?] )
        providers:pyqt6? ( dev-python/PyQt6[python_abis:*(-)?] )
        providers:pyside2? ( dev-python/PySide2[python_abis:*(-)?] )
        providers:pyside6? ( dev-python/PySide6[python_abis:*(-)?] )
"

# Tests are mostly testing availability of PyQt5 parts and need pytest-qt.
# Restrict them as this heavily depends on enabled PyQt options.
RESTRICT="test"

pkg_postinst() {
    einfo "${PN} uses first found provider in order of preference: pyqt5, pyqt6, pyside6, pyside2"
    einfo "To override this, environment variable QT_API can be set to one of these names"
}

