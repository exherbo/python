# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/_}
MY_PNV=${MY_PN}-${PV}

require pypi
require py-pep517 [ backend=hatchling test=pytest work=${MY_PNV} ]

SUMMARY="High level API for internationalizing Python libraries and applications"
DESCRIPTION="The flufl.i18n library provides a convenient API for managing
translation contexts in Python applications. It provides facilities not only
for single-context applications like command line scripts, but also more
sophisticated management of multiple-context applications such as Internet
servers."

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/atpublic[python_abis:*(-)?]
    test:
        dev-python/sybil[python_abis:*(-)?]
"

PYTEST_PARAMS=( test )

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    # Avoid pytest-cov dep
    edo sed \
        -e "s/'--cov=flufl --cov-report=term --cov-report=xml /'/" \
        -i pyproject.toml
}


