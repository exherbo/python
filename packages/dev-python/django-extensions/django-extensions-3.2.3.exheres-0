# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist='2' test=pytest ]

SUMMARY="A collection of custom extensions for the Django Framework"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/Django[>=3.2][python_abis:*(-)?]
    test:
        dev-python/factory-boy[python_abis:*(-)?]
        dev-python/pip[python_abis:*(-)?]
        dev-python/Pygments[python_abis:*(-)?]
        dev-python/pytest-django[python_abis:*(-)?]
        dev-python/shortuuid[python_abis:*(-)?]
        dev-python/Werkzeug[python_abis:*(-)?]
"

# I give up for now, see below for efforts, but pytest also fails on
# upstream CI @ github, unfortunately the logs aren't available anymore
RESTRICT="test"

PYTEST_PARAMS=(
    django_extensions tests
    # Wants to download stuff via pip
    --ignore=tests/management/commands/test_pipchecker.py
    # TODO: No fixture named 'group' found.
    --ignore=tests/management/commands/test_export_emails.py
    --ignore=tests/management/commands/test_set_fake_emails.py
    --ignore=tests/management/commands/test_set_fake_passwords.py
    # Wants to import a literally invalid package
    --ignore=tests/testapp/scripts/invalid_import_script.py
    # These just fail as far as I can tell
    -k "not test_should_return_SyncDataError_when_file_has_non_existent_field_in_fixture_data \
        and not test_should_return_SyncDataError_when_file_not_contains_valid_fixture_data \
        and not test_should_return_SyncDataError_when_multiple_fixtures \
        and not test_should_delete_old_objects_and_load_data_from_json_fixture \
        and not test_should_keep_old_objects_and_load_data_from_json_fixture \
        and not test_should_print_that_there_is_error \
        and not test_validate_templates"
)

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    edo sed \
        -e 's/--cov=django_extensions --cov-report html --cov-report term//' \
        -i setup.cfg
}

