# Copyright 2009-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'dbus-python-0.83.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require meson python [ blacklist=2 ]
require test-dbus-daemon

BASE_URI="https://dbus.freedesktop.org"
SUMMARY="Python bindings for the D-Bus messagebus"
HOMEPAGE="${BASE_URI}/doc/${PN}/"
DOWNLOADS="${BASE_URI}/releases/${PN}/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="${BASE_URI}/doc/${PN}/README"
UPSTREAM_DOCUMENTATION="${BASE_URI}/doc/${PN}/doc [[ lang = en ]]"

SLOT="0"
LICENCES="MIT"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        (
            dev-python/pyproject-metadata[python_abis:*(-)?]
            python_abis:3.8? ( dev-python/tomli[python_abis:3.8] )
            python_abis:3.9? ( dev-python/tomli[python_abis:3.9] )
            python_abis:3.10? ( dev-python/tomli[python_abis:3.10] )
        ) [[ *note = [ generate-pkginfo.py (cf. compile_one_multibuild) ] ]]
        dev-util/patchelf
        virtual/pkg-config
        doc? (
            dev-python/Sphinx[python_abis:*(-)?]
            dev-python/sphinx_rtd_theme[python_abis:*(-)?]
        )
    build+run:
        dev-libs/glib:2[>=2.40]
        sys-apps/dbus[>=1.8.0]
    test:
        gnome-bindings/pygobject:3[python_abis:*(-)?]
"

src_prepare() {
    meson_src_prepare

    edo sed -e "/install_dir: /s/meson.project_name()/\'${PNVR}\'/" \
        -i doc/meson.build
}

configure_one_multibuild() {
    local meson_params=(
        -Dpython=${PYTHON}
        $(meson_switch doc)
        $(expecting_tests -Dtests=true -Dtests=false)
    )

    exmeson ${meson_params[@]}

    # Generate the metadata needed to load it as a python module
    edo ${PYTHON} "${MESON_SOURCE}"/tools/generate-pkginfo.py ${PV} PKG-INFO
}

compile_one_multibuild() {
    meson_src_compile
}

test_one_multibuild() {
    test-dbus-daemon_run-tests meson_src_test
}

install_one_multibuild() {
    meson_src_install

    python_bytecompile

    insinto "$(python_get_sitedir)/dbus_python.egg-info"
    doins PKG-INFO
}

