# Copyright 2009-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist=none ] test-dbus-daemon

BASE_URI="http://dbus.freedesktop.org"
SUMMARY="Python bindings for the D-Bus messagebus"
HOMEPAGE="${BASE_URI}/doc/${PN}/"
DOWNLOADS="${BASE_URI}/releases/${PN}/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="${BASE_URI}/doc/${PN}/README"
UPSTREAM_DOCUMENTATION="${BASE_URI}/doc/${PN}/doc [[ lang = en ]]"

SLOT="0"
LICENCES="MIT"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            dev-python/Sphinx[python_abis:*(-)?]
            dev-python/sphinx_rtd_theme[python_abis:*(-)?]
        )
    build+run:
        dev-libs/glib:2[>=2.40]
        sys-apps/dbus[>=1.8.0]
    test:
        gnome-bindings/pygobject:3[python_abis:*(-)?]
"

configure_one_multibuild() {
    econf \
        PYTHON_VERSION=$(python_get_abi) \
        $(option_enable doc documentation)
}

install_one_multibuild() {
    default
    python_bytecompile
}

