# Copyright 2023-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="${PNV/-/_}"

require pypi [ pnv=${MY_PNV} ] py-pep517 [ backend=hatchling work=${MY_PNV} test=pytest ]

SUMMARY="The JSON Schema meta-schemas and vocabularies, exposed as a Registry"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/hatch_vcs[python_abis:*(-)?]
    build+run:
        dev-python/referencing[>=0.31.0][python_abis:*(-)?]
        python_abis:3.8? ( dev-python/importlib_resources:0[>=1.4.0][python_abis:3.8] )
"

# Do not collect tests from our installed package, tests shouldn't be installed
# but jsonschema-specifications seems to do it anyway.
PYTEST_PARAMS=("--ignore=pep517_tests_${PN}")

