# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

local binaries=(
    myst-anchors
    myst-docutils{,-demo,-html,-html5,-latex,-pseudoxml,-xml}
    myst-inv
)

require github [ user=executablebooks tag=v${PV} ]
require py-pep517 [ blacklist="3.8 3.9" backend=flit_core test=pytest \
    work=MyST-Parser-${PV} entrypoints=[ "${binaries[@]}" ] ]

SUMMARY="An extended commonmark compliant parser, with bridges to docutils/sphinx"
DESCRIPTION="
MyST is a flavor of markdown that is designed for simplicity, flexibility, and
extensibility. This repository serves as the reference implementation of MyST
Markdown, as well as a collection of tools to support working with MyST in
Python and Sphinx. It contains an extended CommonMark-compliant parser using
markdown-it-py, as well as a Sphinx extension that allows you to write MyST
Markdown in Sphinx."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/docutils[>=0.19&<0.22][python_abis:*(-)?]
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/markdown-it-py[>=3.0][python_abis:*(-)?]
        dev-python/mdit-py-plugins[>=0.4.1][python_abis:*(-)?]
        dev-python/PyYAML[python_abis:*(-)?]
        dev-python/Sphinx[>=7&<9][python_abis:*(-)?]
    test:
        dev-python/beautifulsoup4[python_abis:*(-)?]
        dev-python/defusedxml[python_abis:*(-)?]
        dev-python/pytest[>=8.0][python_abis:*(-)?]
        dev-python/pytest-param-files[>=0.6.0][python_abis:*(-)?]
        dev-python/pytest-regressions[python_abis:*(-)?]
        dev-python/sphinx-pytest[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-fix-test-973.patch
    "${FILES}"/${PNV}-Fix-tests-for-latest-sphinx-993.patch
)

PYTEST_PARAMS=(
    -p no:pytest-mypy-plugins
)

PYTEST_SKIP=(
    # Avoid linkify-it-py dep
    test_cmdline[40-linkify]
    test_cmdline[50-gfm-strikethrough]
    test_cmdline[68-gfm-disallowed-html]
    test_cmdline[95-gfm-autolink]
    test_extended_syntaxes
)

