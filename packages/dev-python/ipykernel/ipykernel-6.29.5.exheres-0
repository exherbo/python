# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=hatchling backend_version_spec="[>=1.4]" test=pytest ]

SUMMARY="IPython Kernel for Jupyter"
HOMEPAGE+=" https://ipython.org"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/comm[>=0.1.1][python_abis:*(-)?]
        dev-python/nest-asyncio[python_abis:*(-)?]
        dev-python/debugpy[>=1.6.5][python_abis:*(-)?]
        dev-python/ipython[>=7.23.1][python_abis:*(-)?]
        dev-python/jupyter_client[>=6][python_abis:*(-)?]
        dev-python/jupyter_core[>=4.12][python_abis:*(-)?]
        dev-python/matplotlib-inline[>=0.1][python_abis:*(-)?]
        dev-python/packaging[python_abis:*(-)?]
        dev-python/psutil[python_abis:*(-)?]
        dev-python/pyzmq[>=24][python_abis:*(-)?]
        dev-python/tornado[>=6.1][python_abis:*(-)?]
        dev-python/traitlets[>=5.4.0][python_abis:*(-)?]
    test:
        dev-python/flaky[python_abis:*(-)?]
        dev-python/numpy[python_abis:*(-)?]
        dev-python/pytest-asyncio[>=0.23.5][python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/pytest-timeout[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    # test_async fails on CI, local is fine
    -k 'not test_async and not test_pylab'
)

