# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Based in part upon pyyaml-3.08.ebuild which is:
#   Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools blacklist=3 ]

SUMMARY="YAML parser and emitter for Python"
DESCRIPTION="
PyYAML features
* A complete YAML 1.1 parser. In particular, PyYAML can parse all examples from
the specification. The parsing algorithm is simple enough to be a reference for
YAML parser implementors.
* Unicode support including UTF-8/UTF-16 input/output and \u escape sequences.
* low-level event-based parser and emitter API (like SAX).
* high-level API for serializing and deserializing native Python objects (like DOM or pickle).
* support for all types from the YAML types repository. A simple extension API is provided.
* both pure-Python and fast LibYAML-based parsers and emitters.
* relatively sensible error messages.
"
HOMEPAGE+=" https://pyyaml.org/wiki/${PN}"

LICENCES="MIT"
SLOT="2.7"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        dev-libs/libyaml
"

install_one_multibuild() {
    setup-py_install_one_multibuild

    if option examples; then
        insinto /usr/share/doc/${PNVR}/examples
        doins -r examples/.
    fi
}

