# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

PROJECT="PyDev.Debugger"
MY_PN="pydev_debugger"
MY_PV="${PV//./_}"

# Tarball on pypi doesn't have the tests
require github [ user=fabioz project=${PROJECT} tag=${MY_PN}_${MY_PV} ]
require setup-py [ import=setuptools blacklist='2' test=pytest work=${PROJECT}-${MY_PN}_${MY_PV} ]

SUMMARY="PyDev.Debugger (used in PyDev, PyCharm and VSCode Python)"

LICENCES="|| ( Apache 2.0 EPL-1.0 )"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/numpy[python_abis:*(-)?]
        dev-python/psutil[python_abis:*(-)?]
        dev-python/untangle[python_abis:*(-)?]
    run:
        sys-devel/gdb
"

# Still some failing tests and they take ages
RESTRICT="test"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # Remove some pre-built artifacts
    edo pushd pydevd_attach_to_process
    edo rm *.dll *.exe
    edo rm -r winappdbg windows
    edo popd
}

compile_one_multibuild() {
    edo pushd pydevd_attach_to_process/
    edo ${CXX} ${CXXFLAGS} -m64 -shared -o attach_linux_amd64.so \
        -fPIC ${LDFLAGS} -nostartfiles linux_and_mac/attach.cpp
    edo popd

    setup-py_compile_one_multibuild
}

PYTEST_PARAMS=(
    # Binds to 0.0.0.0@0
    # Avoid Django and flask as tests dependencies
    -k 'not test_edit and not test_completion_sockets_and_messages and not test_case_django and not test_case_flask'
)

