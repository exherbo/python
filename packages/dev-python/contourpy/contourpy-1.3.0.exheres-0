# Copyright 2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=meson-python backend_version_spec="[>=0.13.1]" \
    blacklist="3.8" test=pytest ]

SUMMARY="Library for calculating contours of 2D quadrilateral grids"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-libs/pybind11[>=2.13.4][python_abis:*(-)?]
        sys-devel/meson[>=1.2.0]
        sys-devel/ninja
    build+run:
        dev-python/numpy[>=1.23][python_abis:*(-)?]
    test:
        dev-python/docutils-stubs[python_abis:*(-)?]
        dev-python/Pillow[python_abis:*(-)?]
        dev-python/wurlitzer[python_abis:*(-)?]
    suggestion:
        dev-python/matplotlib[python_abis:*(-)?]
"

PYTEST_SKIP=(
    # Avoid the dependency and that's certainly more interesting for developers of
    # contourpy and may break with new versions of cppcheck.
    test_cppcheck
    # Same is probably true, would also need https://pypi.org/project/bokeh/
    test_mypy
    # Would need pytest-rerunfailures, which we don't have
    test_config_filled_quad_as_tri
)

test_one_multibuild() {
    # Avoid a dependency cycle with matplotlib
    if has_version dev-python/matplotlib[python_abis:$(python_get_abi)] ; then
        py-pep517_run_tests_pytest
    else
        ewarn "dev-python/matplotlib is not yet installed, skipping tests"
    fi
}

