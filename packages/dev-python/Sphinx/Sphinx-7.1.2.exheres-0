# Copyright 2009 Jan Meier
# Distributed under the terms of the GNU General Public License v2

require github [ user=sphinx-doc tag=v${PV} ]
require py-pep517 [ backend=flit_core test=pytest work=${PNV,} \
    entrypoints=[ sphinx-apidoc sphinx-autogen sphinx-build sphinx-quickstart ] ]

SUMMARY="Sphinx is a tool that makes it easy to create intelligent and beautiful documentation"
HOMEPAGE+=" https://www.sphinx-doc.org"

LICENCES="
    BSD-2    [[ note = [ smartypants.py, Sphinx ] ]]
    BSD-3    [[ note = [ SmartyPants_ ] ]]
    MIT      [[ note = [ JQuery ] ]]
    PSF-2.2  [[ note = [ pgen2 ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"

MYOPTIONS="
    doc [[ description = [ Install HTML documentation ] ]]
    ( providers: graphicsmagick imagemagick ) [[
        *description = [ Tests require the convert command ]
        number-selected = at-least-one
    ]]
"

DEPENDENCIES="
    build+run:
        dev-python/alabaster[>=0.7&<0.8][python_abis:*(-)?]
        dev-python/Babel[>=2.9][python_abis:*(-)?]
        dev-python/docutils[>=0.18.1&<0.21][python_abis:*(-)?]
        dev-python/imagesize[>=1.3][python_abis:*(-)?]
        dev-python/Jinja2[>=3.0][python_abis:*(-)?]
        dev-python/packaging[>=21.0][python_abis:*(-)?]
        dev-python/Pygments[>=2.13][python_abis:*(-)?]
        dev-python/requests[>=2.25.0][python_abis:*(-)?]
        dev-python/snowballstemmer[>=2.0][python_abis:*(-)?]
        dev-python/sphinxcontrib-applehelp[python_abis:*(-)?]
        dev-python/sphinxcontrib-devhelp[python_abis:*(-)?]
        dev-python/sphinxcontrib-htmlhelp[>=2.0.0][python_abis:*(-)?]
        dev-python/sphinxcontrib-jsmath[python_abis:*(-)?]
        dev-python/sphinxcontrib-qthelp[python_abis:*(-)?]
        dev-python/sphinxcontrib-serializinghtml[>=1.1.5][python_abis:*(-)?]
    test:
        dev-python/Cython[python_abis:*(-)?]
        dev-python/filelock[python_abis:*(-)?]
        dev-python/html5lib[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        providers:graphicsmagick? ( media-gfx/GraphicsMagick[imagemagick] )
        providers:imagemagick? ( media-gfx/ImageMagick )
    suggestion:
        dev-texlive/texlive-latexextra [[
            description = [ Use the LaTeX builder to create PDF documentation ]
            note = [ framed, threeparttable, titlesec, wrapfig ]
        ]]
"

PYTEST_PARAMS=(
    # fetch from the internet
    --ignore=tests/test_build_{latex,linkcheck}.py
    # FAILED tests/test_ext_autodoc.py::test_cython - AssertionError, last checked: 7.2.6
    -k "not test_cython"
)

compile_one_multibuild() {
    py-pep517_compile_one_multibuild
    if option doc ; then
        # Make sure it uses the new version instead of the old, installed one
        emake \
            SPHINXBUILD="PYTHONPATH=\"$(ls -d ${PWD})\" \$(PYTHON) ../sphinx/cmd/build.py" \
            -C doc html
    fi
}

install_one_multibuild() {
    py-pep517_install_one_multibuild

    # Install HTML docs excluding intersphinx file
    if option doc; then
        edo pushd doc/_build
        edo rm html/objects.inv
        dodoc -r html
        edo popd
    fi
}

