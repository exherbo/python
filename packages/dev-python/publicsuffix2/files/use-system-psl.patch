Upstream: no
Reason: use publicsuffix data from the system package
Source: Debian, see below, adjusted not to use the legacy path

Author: Scott Kitterman <scott@kitterman.com>
Last-Update: 2020-01-24

Index: python-publicsuffix2/src/publicsuffix2/__init__.py
===================================================================
--- python-publicsuffix2.orig/src/publicsuffix2/__init__.py
+++ python-publicsuffix2/src/publicsuffix2/__init__.py
@@ -62,7 +62,7 @@ ABOUT_PSL_FILE = path.join(BASE_DIR, 'pu
 
 class PublicSuffixList(object):
 
-    def __init__(self, psl_file=None, idna=True):
+    def __init__(self, psl_file='/usr/share/publicsuffix/public_suffix_list.dat', idna=True):
         """
         Read and parse a public suffix list. `psl_file` is either a file
         location string, or a file-like object, or an iterable of lines from a
@@ -78,11 +78,14 @@ class PublicSuffixList(object):
 
         The file format is described at http://publicsuffix.org/
 
-        :param psl_file: string or None
+        :param psl_file: string or None - In Debian defaults to system PSL
         :param idna: boolean, whether to convert file to IDNA-encoded strings
         """
         # Note: we test for None as we accept empty lists as inputs
         if psl_file is None or isinstance(psl_file, str):
+            # Debian uses system PSL vice vendored copy.
+            if psl_file is None:
+                psl_file='/usr/share/publicsuffix/public_suffix_list.dat'
             with codecs.open(psl_file or PSL_FILE, 'r', encoding='utf8') as psl:
                 psl = psl.readlines()
         else:
@@ -316,7 +319,7 @@ class PublicSuffixList(object):
 _PSL = None
 
 
-def get_sld(domain, psl_file=None, wildcard=True, idna=True, strict=False):
+def get_sld(domain, psl_file='/usr/share/publicsuffix/public_suffix_list.dat', wildcard=True, idna=True, strict=False):
     """
     Return the private suffix or SLD for a `domain` DNS name string. The
     original publicsuffix2 library used the method get_public_suffix() for this
@@ -327,8 +330,8 @@ def get_sld(domain, psl_file=None, wildc
     location string, or a file-like object, or an iterable of lines from a
     public suffix data file.
 
-    If psl_file is None, the vendored file named "public_suffix_list.dat" is
-    loaded. It is stored side by side with this Python package.
+    psl_file defaults to the Debian system PSL.  The publicsuffix2 vendored
+    copy is not provided in Debian.
 
     The file format is described at http://publicsuffix.org/
     """
@@ -337,7 +340,7 @@ def get_sld(domain, psl_file=None, wildc
     return _PSL.get_sld(domain, wildcard=wildcard, strict=strict)
 
 
-def get_tld(domain, psl_file=None, wildcard=True, idna=True, strict=False):
+def get_tld(domain, psl_file='/usr/share/publicsuffix/public_suffix_list.dat', wildcard=True, idna=True, strict=False):
     """
     Return the TLD or public suffix for a `domain` DNS name string. (this is
     actually the private suffix that is returned) Convenience function that
@@ -347,8 +350,8 @@ def get_tld(domain, psl_file=None, wildc
     location string, or a file-like object, or an iterable of lines from a
     public suffix data file.
 
-    If psl_file is None, the vendored file named "public_suffix_list.dat" is
-    loaded. It is stored side by side with this Python package.
+    psl_file defaults to the Debian system PSL.  The publicsuffix2 vendored
+    copy is not provided in Debian.
 
     The file format is described at http://publicsuffix.org/
     """
@@ -357,7 +360,7 @@ def get_tld(domain, psl_file=None, wildc
     return _PSL.get_tld(domain, wildcard=wildcard, strict=strict)
 
 
-def get_public_suffix(domain, psl_file=None, wildcard=True, idna=True, strict=False):
+def get_public_suffix(domain, psl_file='/usr/share/publicsuffix/public_suffix_list.dat', wildcard=True, idna=True, strict=False):
     """
     Included for compatibility with the original publicsuffix2 library -- this
     function returns the private suffix or SLD of the domain. To get the public
@@ -368,8 +371,8 @@ def get_public_suffix(domain, psl_file=N
     location string, or a file-like object, or an iterable of lines from a
     public suffix data file.
 
-    If psl_file is None, the vendored file named "public_suffix_list.dat" is
-    loaded. It is stored side by side with this Python package.
+    psl_file defaults to the Debian system PSL.  The publicsuffix2 vendored
+    copy is not provided in Debian.
 
     The file format is described at http://publicsuffix.org/
     """
Index: python-publicsuffix2/setup.py
===================================================================
--- python-publicsuffix2.orig/setup.py
+++ python-publicsuffix2/setup.py
@@ -94,11 +94,11 @@ setup(
     packages=find_packages('src'),
     package_dir={'': 'src'},
     py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
-    include_package_data=True,
+    include_package_data=False,
     zip_safe=False,
-    setup_requires = [
-        'requests >= 2.7.0',
-    ],
+    #setup_requires = [
+    #    'requests >= 2.7.0',
+    #],
     classifiers=[
         'Intended Audience :: Developers',
         'License :: OSI Approved :: MIT License',
@@ -113,5 +113,5 @@ setup(
     keywords=[
         'domain', 'public suffix', 'suffix', 'dns', 'tld', 'sld', 'psl', 'idna',
     ],
-    cmdclass={'update_psl': UpdatePslCommand},
+   # cmdclass={'update_psl': UpdatePslCommand},
 )
