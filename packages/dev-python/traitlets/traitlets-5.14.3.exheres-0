# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require pypi py-pep517 [ backend=hatchling backend_version_spec="[>=1.5]" test=pytest ]

SUMMARY="A lightweight Traits like module"
DESCRIPTION="
Framework that lets Python classes have attributes with type checking,
dynamically calculated default values, and ‘on change’ callbacks
"
HOMEPAGE+=" https://traitlets.readthedocs.io/"
LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# TODO: unpackaged test dependencies: pre-commit, pytest-mypy-testing
DEPENDENCIES="
    test:
        dev-python/argcomplete[>=3.0.3][python_abis:*(-)?]
        dev-python/mypy[>=1.7.0][python_abis:*(-)?]
        dev-python/pytest-mock[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    # skip tests depending on unpackaged test dependency pytest-mypy-testing
    --ignore=tests/test_typing.py
)

PYTEST_SKIP=(
    # Skip a test, which broke with Python 3.12.7+
    # https://github.com/ipython/traitlets/issues/911
    test_complete_custom_completers
)

