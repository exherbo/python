# Copyright 2015 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ blacklist=2 import=setuptools test=pytest ]
# The tests use the git repo as reference data, so we use scm-git even for releases
# NOTE(moben): maybe extend github.exlib to allow using git for releases as well, but so far only
# gitdb and GitPython need it
SCM_TAG=${PV}
SCM_REPOSITORY="https://github.com/gitpython-developers/${PN}"
SCM_EXTERNAL_REFS="${PN}/ext/smmap:"
require scm-git

SUMMARY="A pure-Python git object database"
DESCRIPTION="
GitDB allows you to access bare git repositories for reading and writing. It aims at allowing full
access to loose objects as well as packs with performance and scalability in mind. It operates
exclusively on streams, allowing to handle large objects with a small memory footprint."
HOMEPAGE="https://github.com/gitpython-developers/${PN}"
REMOTE_IDS="github:gitpython-developers/${PN}"
UPSTREAM_DOCUMENTATION="http://${PN}.readthedocs.org"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/smmap[>=3.0.1&<6][python_abis:*(-)?]
    test:
        dev-scm/git
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # copy objects from ${SCM_HOME}/${CHECKOUT_TO}, to give the tests something to work with
    edo git repack -adf
}
