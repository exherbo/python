# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require github [ user=hamcrest tag=V${PV} ]
require py-pep517 [ backend=hatchling test=pytest ]

SUMMARY="Hamcrest framework for matcher objects"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/hatch_vcs[python_abis:*(-)?]
    test:
        dev-python/mock[python_abis:*(-)?]
        dev-python/PyYAML[python_abis:*(-)?]
        dev-python/types-mock[python_abis:*(-)?]
"

compile_one_multibuild() {
    SETUPTOOLS_SCM_PRETEND_VERSION=${PV} py-pep517_compile_one_multibuild
}

PYTEST_SKIP=(
    # Broken with numpy >= 1.24.0
    # https://github.com/hamcrest/PyHamcrest/issues/228
    test_numpy_numeric_type_complex
    test_numpy_numeric_type_float
    test_numpy_numeric_type_int
)

PYTEST_PARAMS=(
    # Broken with mypy >= 1.10.1
    # https://github.com/hamcrest/PyHamcrest/pull/250/
    --ignore=tests/type-hinting
)

