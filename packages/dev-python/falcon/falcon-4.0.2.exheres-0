# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=falconry ]
require py-pep517 [ backend=setuptools test=pytest entrypoints=[ ${PN}-{bench,inspect-app,print-routes} ] ]

SUMMARY="A reliable, high-performance Python web framework"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/Cython[>=3.0.8][python_abis:*(-)?]
    test:
        dev-python/aiofiles[python_abis:*(-)?]
        dev-python/asgiref[python_abis:*(-)?]
        dev-python/cbor2[python_abis:*(-)?]
        dev-python/httpx[python_abis:*(-)?]
        dev-python/msgpack
        dev-python/mujson[python_abis:*(-)?]
        dev-python/pytest-asyncio[python_abis:*(-)?]
        dev-python/pytest-runner[python_abis:*(-)?]
        dev-python/PyYAML[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/testtools[python_abis:*(-)?]
        dev-python/ujson[python_abis:*(-)?]
        dev-python/uvicorn[>=0.17.0][python_abis:*(-)?]
        dev-python/websockets[python_abis:*(-)?]
"
# NOTE: There's an optional test dependency on dev-python/rapidjson

PYTEST_PARAMS=(
    --ignore tests/test_wsgi_servers.py
    --ignore tests/asgi/test_cythonized_asgi.py
)
PYTEST_SKIP=(
    test_imported_from_c_modules
    test_stream_has_private_read
)

test_one_multibuild() {
    # tox.ini
    export FALCON_DISABLE_CYTHON=Y

    py-pep517_test_one_multibuild
}

