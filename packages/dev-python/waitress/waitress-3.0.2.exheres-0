# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=setuptools blacklist="3.8" test=pytest entrypoints=[ ${PN}-serve ] ]

SUMMARY="Waitress WSGI server"
DESCRIPTION="
Waitress is a production-quality pure-Python WSGI server with very acceptable
performance. It has no dependencies except ones which live in the Python
standard library. It runs on CPython on Unix and Windows under Python 3.7+.
It supports HTTP/1.0 and HTTP/1.1."

LICENCES="ZPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~arm"
MYOPTIONS=""

DEPENDENCIES="
"

PYTEST_SKIP=(
    # Wants to bind to 0.0.0.0@1234
    test_backward_compatibility
)

prepare_one_multibuild() {
    py-pep517_prepare_one_multibuild

    # Not interesting to us and avoids coverage and pytest-cov deps
    edo sed -e '/addopts = --cov -W always/d' -i setup.cfg
}

test_one_multibuild() {
    _PYWORK="${WORKBASE}/${MULTIBUILD_CLASS}/${MULTIBUILD_TARGET}/${PNV}"

    esandbox allow_net "unix:/tmp/waitress.test*"
    esandbox allow_net "unix:${_PYWORK}/@test_*_tmp"

    py-pep517_test_one_multibuild

    esandbox disallow_net "unix:${_PYWORK}/@test_*_tmp"
    esandbox disallow_net "unix:/tmp/waitress.test-*"
}

